ar_tags_gazebo
===============================

## Usage

```xml
<?xml version="1.0" ?>
<launch>
  <include file="$(find gazebo_ros)/launch/empty_world.launch">
    <env name="GAZEBO_MODEL_PATH" value="$(optenv GAZEBO_MODEL_PATH):$(find ar_tags_gazebo)/models"/>
  </include>
</launch>
```